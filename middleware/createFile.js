
const fs = require('fs');

module.exports = function (req, res) {
    const fileName = req.body.filename;
    const content = req.body.content;
    
    if (!req.body.content) {
        res.status(400).json({ message: 'Please specify "content" parameter' });
        return;
    }

    fs.writeFile(`./static/${fileName}`, content, (err) => {
        if (err) {
            console.log(err);
            return res.status(500).json({ message: `Server error` });
        }
    });
    res.status(200).json({ message: 'File created successfully' });
    console.log('File created');
}

