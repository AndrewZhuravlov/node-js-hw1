const fs = require('fs');

module.exports = function (req, res, next) {
    
    fs.readdir('./static', (err, items) => {
        if(err) {
            console.log("Something broke again");
            return res.status(500).json({message: 'Server error'});
        }
        if(items.includes(req.body.filename)) {
            console.log('File already exist');
            res.status(400).json({message: "File already exist"});
        }else{
            next();
        }
    });

}