const fs = require('fs');


module.exports = function (req, res) {
    console.log(req.params.filename);
    fs.readdir('./static', (err, files) => {
        if (err) {
            console.log("Something broke again");
            throw err
        } else if (!files.length) {
            res.status(200).json({ message: "No files" });
            console.log("The directory doesn't have files");

            return;
        }

        res.status(200).json({
            message: "Success",
            files
        });
        console.log("Files have been sent!");
    });
}

