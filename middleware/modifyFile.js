const fs = require('fs');

module.exports = function(req, res, next) {
    const newContent = req.body.content;
    if(!newContent) {
        res.status(400).json({message: 'Please specify "new content" parameter'});
        return;
    }

    fs.writeFile(`./static/${req.fileName}`, req.body.content, err=>{
        if(err) {
            console.log("Something broke again");
            return res.status(500).json({message: 'Server error'});
        }
    });
    res.status(200).json({message: "File has bee updated."});
    console.log("File has bee updated.");
}