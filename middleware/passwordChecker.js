const fs = require('fs');

module.exports = function(req, res, next) {
    const requireFile = req.params.filename;
    const password = req.query.pswd;
    
    fs.readFile('./file-base.json', 'utf-8', (err, data)=> {
        if (err) {
            console.log(err);
            return res.status(500).json({ message: `Server error` });
        }

        const log = JSON.parse(data);
        const fileInfo = log.find(item => item.fileName === requireFile);
        
        if(!fileInfo) {
            res.status(400).json({message: `No file with '${requireFile}' filename found`});
            return;
        }

        if(!fileInfo.password) {
            console.log('Password does not required.');
            req.fileName = fileInfo.fileName;
            next();
            return;
        }else{
            if(fileInfo.password === password) {
                console.log('Access open.');
                req.fileName = fileInfo.fileName;
                next();
            }else{
                res.status(400).json({message: 'Invalid password.'});
                console.log('Invalid password.');
            }
        } 
    });
}