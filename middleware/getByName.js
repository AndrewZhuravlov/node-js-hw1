const fs = require('fs');

module.exports = function(req, res) {
    const ext = req.fileName.slice(req.fileName.indexOf('.') + 1);
    const obj = {
        message: 'Success',
        filename: req.fileName,
        extension: ext,
        content: fs.readFileSync(`./static/${req.fileName}`, 'utf-8'),
        uploadedDate: fs.statSync(`./static/${req.fileName}`).birthtime
    }
    console.log('File has been sent');
    res.status(200).json(obj);
}