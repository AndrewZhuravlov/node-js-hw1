const fs = require('fs');

module.exports = function (req, res) {
    if (fs.existsSync(`./static/${req.fileName}`)) {
        fs.unlink(`./static/${req.fileName}`, err => {
            if (err) {
                console.log("Something broke again");
                throw err;
            }
        });

        const data = fs.readFileSync('./file-base.json', 'utf-8', err=>{
            if (err) {
                console.log("Something broke again");
                throw err;
            }
        });

        const bd = JSON.parse(data);
        const idx = bd.findIndex(item => item.fileName === req.fileName);
        bd.splice(idx, 1);

        fs.writeFile('./file-base.json', JSON.stringify(bd), err => {
            if (err) {
                console.log("Something broke again");
                throw err;
            }
        })

        res.status(200).json({ message: "File has been deleted." });
        console.log("File has been deleted.");
    } else {
        res.status(400).json({ message: "File do not exist" });
        console.log("File do not exist");
    }
}