const fs = require('fs');

module.exports = function (req, res, next) {
    const fileName = req.body.filename;
    const password = req.body.password || '';
    const obj = { fileName, password };

    fs.readdir('./', (err, items) => {
        if(err) {
            console.log("Something broke again");
            return res.status(500).json({ message: `Server error` }); 
        }

        if (items.includes('file-base.json')) {

            fs.readFile('./file-base.json', 'utf-8', (err, data) => {
                
                if(err) {
                    console.log("Something broke again");
                    return res.status(500).json({ message: `Server error` }); 
                }

                const bd = JSON.parse(data);

                if (bd.find(item => item.fileName === fileName)) {
                    res.status(400).json({message: 'The file has already been logged!'});
                    console.log('The file has already been logged!');
                    
                    return;
                }

                bd.push(obj);
                writeFileHandler(bd);
                next();
            });

        } else {
            writeFileHandler([obj]);
            next();
        }
        console.log('File logged successfully');
    });
}

function writeFileHandler(data) {
    fs.writeFile('./file-base.json', JSON.stringify(data), (err) => {
        if(err) {
            console.log("Something broke again");
            return res.status(500).json({ message: `Server error` }); 
        }
    });
}
