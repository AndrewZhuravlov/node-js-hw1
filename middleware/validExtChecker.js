
module.exports = function (req, res, next) {
    const index = req.body.filename.lastIndexOf('.');
    const ext = req.body.filename.slice(index + 1);
    const f = req.body.filename.slice(0, index);
    const validExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
    
    if (!f || index === -1) {
        return res.status(400).json({ "message": "Please specify 'filename' parameter" });
    }

    if (validExt.includes(ext)) {
        console.log('File passed validation');
        next();
    } else {
        res.status(400).json({ message: 'Invalid file type!' });
        console.log('Invalid file type');
    }
}