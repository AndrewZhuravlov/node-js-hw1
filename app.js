const express = require('express');
const app = express();
const createFile = require('./middleware/createFile');
const getAllFiles = require('./middleware/getAllFiles');
const doesFileExist = require('./middleware/doesFileExist');
const getByName = require('./middleware/getByName');
const logFile = require('./middleware/logFile');
const validExtChecker = require('./middleware/validExtChecker');
const passwordChecker = require('./middleware/passwordChecker');
const modifyFile = require('./middleware/modifyFile');
const deleteFile = require('./middleware/deleteFile');
const createdDirectory = require('./helpers/createDirectory');

createdDirectory('./static');
app.use(express.json());
app.post('/api/files', doesFileExist, validExtChecker, logFile, createFile);
app.get('/api/files/:filename', passwordChecker, getByName);
app.put('/api/files/:filename', passwordChecker, modifyFile);
app.delete('/api/files/:filename', passwordChecker, deleteFile);
app.get('/api/files', getAllFiles);
app.all('*', (req, res)=> {
    res.status(400).json({message: 'Bad request! 400'})
})

app.use((err, req, res, next) => {
    if (err) {
        console.error(err.stack);
        return res.status(500).json({ message: `Server error` });
    }
});


module.exports = app;