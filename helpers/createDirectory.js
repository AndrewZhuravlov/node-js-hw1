const fs = require('fs');

module.exports = function (dirName) {
    if (fs.existsSync(dirName)) {
        return;
    }

    try {
        fs.mkdirSync(dirName, { recursive: true })
    } catch (err) {
        if (err.code !== 'EEXIST') {
            console.log(err);
        }

        return res.status(500).json({ message: `Server error` });
    }
}